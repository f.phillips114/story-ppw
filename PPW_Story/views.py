from django.http import HttpResponse
from django.shortcuts import render, redirect
# from django.shortcuts import render

def index(request):
    response = {}
    return render(request,"index.html", response)
def portfolio(request):
    response = {}
    return render(request,"portfolio.html", response)
def story1(request):
    response = {}
    return render(request,"story1.html", response)
def story3(request):
    response = {}
    return render(request,"story3.html", response)
def resume(request):
    response = {}
    return render(request,"resume.html", response)
